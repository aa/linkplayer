THINGS TO COME:
* SAVEABLE
* CONTENT 
* VERSIONS+LINKS
* CROP TOOL (FRAGMENT)
* EVENTS


## SAVEABLE

Buttons to switch between different sources / versions / branches.
Ability to save page source in various ways (as text dump, email?, ... to gitlab)


## VERSIONS+LINKS

Grouped links to various versions of a file (image original, resizes, evt. filtered / other output?!)
Backlinks / ring links for a particular media

Currently, a thumbnail *img* , wrapped with an anchor linking to a "large/medium" version.


## CONTENT 

Listing of tag hierarchy including (editable) attributes

## CROP TOOL (FRAGMENT)

Adjusts fragment in element href.

## EVENTS: loaded, ended, pause play ... etc.
(then... how to connect events to controls...

Evt. event chaining?

Photos can be given a temporal dimension.

# Notes

In general, all edits have impact on document.
("autoplay" via onended attribute --- on link -- "automagically" transposed to the player ?!)
final document can be PLAYED... An automatic / default "autoplay" behaviour would be pleasant!

Evt. need to reselect (periodically) to deal with DOM changes (or better can (try) to watch for DOM changes ?!)

Eventually remake how to breakdance like behaviours!?

How would (sub)titles / SRT style annotations work? Are there titlechange events?


Next to this
=================
* Mixin draggable / reorderable (jquery ui?!)
* PAGE VERSIONS: git-enabled widget for versions / (personal) branches / fork the page to make different page versions.
* Link to different "page interpreters" ... that then translate extracted json-ld into melt commands, ZIP and download high resolution / originals.

* draggable for player <!> (via jquery then ;)
* Annotations... Comments / etc / "likes" etc.

* Try in (music) directory listings!
