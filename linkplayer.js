(function (window) {
/*

linkplayer: A "lightbox" alternative

*/


function linkplayer (elt, opts) {
	if (opts === undefined) {
		opts = {};
	}
	i
	var selection = opts.selection,
		selection_text;
	if (typeof(selection) == "string") {
		selection_text = selection;
		selection = document.querySelectorAll(selection_text);
	}
	if (!Array.isArray(selection)) {
		selection = Array.prototype.slice.apply(selection);
	}
	var player = elt, // document.createElement("div"),
		controls = document.createElement("div"),
		nextdiv = document.createElement("div"),
		prevdiv = document.createElement("div"),
		info = document.createElement("input"),
		cur,
		seltext = document.createElement("input"),
		/// playbutton = document.createElement("button"),
		body = document.createElement("div"),
		iframe = document.createElement("iframe"),
		inFocus = false;

	info.setAttribute("type", "text");
	info.classList.add("lpinfo");
	info.addEventListener("focus", function () {
		// console.log("focus");
		inFocus = true;
	})
	info.addEventListener("blur", function () {
		// console.log("blur");
		inFocus = false;
	})

	function url_filename (x) {
		var pat = /\/([^\/]+)$/.exec(x);
		if (pat) {
			return pat[1];
		}
	}

	function update_info () {
		var curindex,
			len = selection.length,
			label,
			fname = '';

		if (cur) {
			curindex = selection.indexOf(cur);
			fname = url_filename(this.href) || "";
			label = (curindex+1) + "/";
		} else {
			label = "-/";
		}
		label += selection.length;

		label = fname + " " + label;

		info.value = label;
	}

	player.classList.add("linkplayer");
	controls.classList.add("lpcontrols");
	body.classList.add("lpbody");

	function next () {
		if (cur) {
			var curindex = selection.indexOf(cur);
			if ((curindex+1) < selection.length) {
				play(selection[curindex+1]);
			}
		} else if (selection.length > 0) {
			play(selection[0])
		}
	}
	nextdiv.addEventListener("click", next);
	nextdiv.classList.add("lpnext");

	function prev () {
		if (cur) {
			var curindex = selection.indexOf(cur);
			if ((curindex-1) >= 0) {
				play(selection[curindex-1]);
			}
		} else if (selection.length > 0) {
			play(selection[selection.length-1]);
		}
	}
	prevdiv.addEventListener("click", prev);
	prevdiv.classList.add("lpprev");

	player.appendChild(body);
	player.appendChild(nextdiv);
	player.appendChild(prevdiv);
	player.appendChild(controls);

	seltext.setAttribute("type", "text");
	if (selection_text) {
		seltext.value = selection_text;
	}
	seltext.classList.add("lpseltext");

	controls.appendChild(info);
	update_info();

	body.appendChild(iframe);

	iframe.addEventListener("load", function () {
		// console.log("iframe load");
		var media = iframe.contentDocument.querySelector("audio,video");
		if (media) {
			media.addEventListener("ended", function () {
				console.log("media ended");
				next();
			})
		}
		var body = iframe.contentDocument.body;
		body.style.margin = "0";
		body.style.textAlign = "center";

	})


	function play (item) {
		if (cur) {
			cur.classList.remove("lpplaying");
		}
		cur = item;
		item.classList.add("lpplaying");
		iframe.src = item.href;

		// ENSURE LINK IS (JUST) ON SCREEN
		var windowHeight = window.innerHeight,
			cbr = item.getBoundingClientRect(),
			itemHeight = (cbr.bottom - cbr.top),
			ptop = (cbr.top + document.body.scrollTop);

		if (cbr.top < 0) { // link is offscreen above... scroll to make it top
			document.body.scrollTop = ptop - 10;
		} else if ((cbr.top + itemHeight) >= windowHeight) { // link is offscreen low, scroll to make it bottom
			document.body.scrollTop = (ptop - windowHeight + itemHeight + 10);
		}

		update_info();
	}


	function itemclick(e) {
		play(this);
		e.preventDefault();	
	}

	for (var i=0; i<selection.length; i++) {
		// console.log(i, selection[i].href);
		var a = selection[i];
		a.addEventListener("click", itemclick);
		// text += url_filename(a.href) + "\n";
		var a2 = document.createElement("a");
		a2.innerHTML = url_filename(a.href);
		a2.href = a.href;
	}

	var KEY_LEFT = 37,
		KEY_UP = 38,
		KEY_RIGHT = 39,
		KEY_DOWN = 40;

	document.addEventListener("keydown", function (e) {
		// console.log('keydown', e.target, e);
		if (inFocus) {
			if (e.keyCode == KEY_LEFT || e.keyCode == KEY_UP)  {
				prev();
			} else if (e.keyCode == KEY_RIGHT || e.keyCode == KEY_DOWN) {
				next();
			}

		}
	})

}

// not sure about jquery integration for now (oct 2016)
// if (window.jQuery) {
// 	jQuery.fn.linkplayer = function (opts) { linkplayer({
// 		selection: this.get()
// 	}); }
// }

// Add this code to the page to 
// document.addEventListener("DOMContentLoaded", function () {
// 	linkplayer({ selection: "a" });
// });
window.linkplayer = linkplayer;


})(window);